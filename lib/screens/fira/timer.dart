import 'dart:async';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class Timers extends StatefulWidget {
  @override
  _TimersState createState() => _TimersState();
}

class _TimersState extends State<Timers> {
  int hour = 0;
  int minute = 0;
  int seconds = 0;
  bool mulai = true;
  bool berhenti = true;
  int timeforTimer = 0;
  String timeTodisplay = "";
  bool checktimer = true;
  void start() {
    setState(() {
      mulai = false;
      berhenti = false;
    });
    timeforTimer = (hour * 60 * 60) + (minute * 60) + seconds;
    Timer.periodic(
        Duration(
          seconds: 1,
        ), (timer) {
      setState(() {
        if (timeforTimer < 1 || checktimer == false) {
          timer.cancel();
          checktimer = true;
          timeTodisplay = (timeforTimer < 1) ? "0" : timeforTimer.toString();
          mulai = true;
          berhenti = true;
        } else if (timeforTimer < 60) {
          timeTodisplay = timeforTimer.toString();
          timeforTimer = timeforTimer - 1;
        } else if (timeforTimer < 3600) {
          int m = timeforTimer ~/ 60;
          int s = timeforTimer - (60 * m);
          timeTodisplay = m.toString() + ":" + s.toString();
          timeforTimer = timeforTimer - 1;
        } else {
          int h = timeforTimer ~/ 3600;
          int t = timeforTimer - (3600 * h);
          int m = t ~/ 60;
          int s = t - (60 * m);
          timeTodisplay =
              h.toString() + ":" + m.toString() + ":" + s.toString();
          timeforTimer = timeforTimer - 1;
        }
      });
    });
  }

  void stop() {
    setState(() {
      mulai = true;
      berhenti = true;
      checktimer = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 70),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          "Jam",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                      NumberPicker.integer(
                        initialValue: hour,
                        minValue: 0,
                        maxValue: 23,
                        onChanged: (val) {
                          setState(() {
                            hour = val;
                          });
                        },
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          "Menit",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                      NumberPicker.integer(
                        initialValue: minute,
                        minValue: 0,
                        maxValue: 60,
                        onChanged: (val) {
                          setState(() {
                            minute = val;
                          });
                        },
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          "Detik",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                      NumberPicker.integer(
                        initialValue: seconds,
                        minValue: 0,
                        maxValue: 60,
                        onChanged: (val) {
                          setState(() {
                            seconds = val;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 60,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Center(
                child: Text(
                  timeTodisplay,
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 80,
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: mulai ? start : null,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        padding: EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 12,
                        ),
                        color: Colors.green,
                        child: Text(
                          "Mulai",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      RaisedButton(
                        onPressed: berhenti ? null : stop,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        padding: EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 12,
                        ),
                        color: Colors.redAccent,
                        child: Text(
                          "Stop",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
