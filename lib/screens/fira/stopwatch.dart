import 'dart:async';

import 'package:flutter/material.dart';

class StopWatch extends StatefulWidget {
  @override
  _StopWatchState createState() => _StopWatchState();
}

class _StopWatchState extends State<StopWatch> {
  bool mulai = true;
  bool berhenti = true;
  bool reset = true;
  String tampilanWaktuAwal = "00:00:00";
  final durasi = Duration(seconds: 1);
  var stopwatch = Stopwatch();
  void starttimer() {
    Timer(durasi, keeprunning);
  }

  void keeprunning() {
    if (stopwatch.isRunning) {
      starttimer();
    }
    setState(() {
      tampilanWaktuAwal = stopwatch.elapsed.inHours.toString().padLeft(2, "0") +
          ":" +
          (stopwatch.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
          ":" +
          (stopwatch.elapsed.inSeconds % 60).toString().padLeft(2, "0");
    });
  }

  void startstopwatch() {
    setState(() {
      berhenti = false;
      mulai = false;
    });
    stopwatch.start();
    starttimer();
  }

  void pausestopwatch() {
    setState(() {
      berhenti = false;
      reset = true;
      stopwatch.start();
      starttimer();
    });
  }

  void stopstopwatch() {
    setState(() {
      berhenti = true;
      reset = false;
    });
    stopwatch.stop();
  }

  void resetstopwatch() {
    setState(() {
      mulai = true;
      reset = true;
    });
    stopwatch.reset();
    tampilanWaktuAwal = "00:00:00";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 150),
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Text(
                tampilanWaktuAwal,
                style: TextStyle(
                  fontSize: 70.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        onPressed: reset ? null : resetstopwatch,
                        padding: EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 12,
                        ),
                        child: Icon(
                          Icons.replay,
                          size: 40,
                        ),
                      ),
                      FlatButton(
                        onPressed: mulai ? startstopwatch : pausestopwatch,
                        padding: EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 12,
                        ),
                        child: Icon(
                          berhenti
                              ? Icons.play_circle_outline
                              : Icons.pause_circle_filled_outlined,
                          size: 40,
                        ),
                      ),
                      FlatButton(
                        onPressed: berhenti ? null : stopstopwatch,
                        padding: EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 12,
                        ),
                        child: Icon(
                          Icons.stop_circle_outlined,
                          size: 40,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
