import 'package:flutter/material.dart';
import './timer.dart' as timer;
import './stopwatch.dart' as stopwatch;

class TimerApp extends StatefulWidget {
  @override
  _TimerAppState createState() => _TimerAppState();
}

class _TimerAppState extends State<TimerApp>
    with SingleTickerProviderStateMixin {
  TabController tabbar;
  @override
  void initState() {
    super.initState();
    tabbar = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TimerApp'),
        backgroundColor: Colors.indigo,
        bottom: TabBar(
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.av_timer),
              text: "Timer",
            ),
            Tab(
              icon: Icon(Icons.access_alarm),
              text: "Stopwatch",
            ),
          ],
          labelStyle: TextStyle(
            fontSize: 18,
          ),
          controller: tabbar,
        ),
      ),
      body: TabBarView(controller: tabbar, children: <Widget>[
        timer.Timers(),
        stopwatch.StopWatch(),
      ]),
    );
  }
}
