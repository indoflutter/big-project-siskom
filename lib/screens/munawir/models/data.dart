const watchData = [
  {
    'gambar':
        'http://img.bukabuku.net/product_thumb/d/c/dc0776e27707362a387258aad845ff43.jpg',
    'nama': 'Sepertiga Malam',
    'harga': '40000',
    'warna': 'Putih',
    'tebal': '100',
    'sinopsis':
        'Apakah salah mencintai pria yang pernah bersalah? Apakah salah menyayangi pria yang sering melakukan kesalahan? Dia adalah Yusuf. Orang yang dulu aku benci, kini aku cintai. Dia datang membawa kepastian, berkata dengan mantap, “Maukah kau menikah denganku?” Apakah kalian pernah merasakan hal yang sama sepertiku? Mencintai seseorang yang salah, di waktu yang salah. Meski aku tahu, rencana Allah SWT tidak pernah salah. Hingga akhirnya, masa lalu itu datang, saat aku dan Yusuf memutuskan untuk saling menerima. Wanita itu datang dan mengantarkan hatiku pada sebuah kebimbangan: bertahan dengan pilihanku sendiri atau menyerah dan melepaskan pria yang kucintai?.',
    'rating': '3.5'
  },
  {
    'gambar':
        'http://img.bukabuku.net/product_thumb/d/7/d7c47bba69beb084c78a35c122b3f8a2.jpg',
    'nama': 'Belajar Website',
    'harga': '30000',
    'warna': 'Polos',
    'tebal': '130',
    'sinopsis':
        'CV. Jubilee Solusi Enterprise adalah sebuah media content provider. Dengan kerja sama dengan PT Elex Media Komputindo (dan beberapa rekanan lain seperti PT Info Media Sarana-Majalah Info Komputer dan RuangKerja Software Engineering), sejak tahun 2005 Jubilee Enterprise telah memproduksi buku komputer dalam skala besar, ratusan buku baru per tahun yang tersebar dari Sabang hingga Merauke.',
    'rating': '3.5'
  },
  {
    'gambar':
        'http://img.bukabuku.net/product_thumb/8/2/82915a23852468c08bf5ee7538e71d74.jpg',
    'nama': 'TPS UTBK SBMPTN',
    'harga': '50000',
    'warna': 'Putih',
    'tebal': '120',
    'sinopsis':
        'Ujian Tulis Berbasis Komputer (UTBK) merupakan tes masuk ke perguruan tinggi yang dilaksanakan oleh Lembaga Tes Masuk Perguruan Tinggi (LTMPT) sebagai satu-satunya lembaga penyelenggara tes perguruan tinggi terstandar di Indonesia.',
    'rating': '4.5'
  }
];

const profileImg = 'https://img.icons8.com/clouds/100/000000/book.png';

const collectionData = [
  {
    'kategori': 'Komputer',
    'GambarKategori':
        'https://images.unsplash.com/photo-1517694712202-14dd9538aa97?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
  },
  {
    'kategori': 'Novel',
    'GambarKategori':
        'https://images.unsplash.com/photo-1586974722938-ffc9efea5a5c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
  },
  {
    'kategori': 'Pendidikan',
    'GambarKategori':
        'https://images.unsplash.com/photo-1503676260728-1c00da094a0b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=709&q=80',
  },
  {
    'kategori': 'Sains',
    'GambarKategori':
        'https://images.unsplash.com/photo-1507413245164-6160d8298b31?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
  },
];
