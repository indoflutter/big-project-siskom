import 'package:flutter/material.dart';

class Peringkat extends StatelessWidget {
  final String rating;

  Peringkat({this.rating});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        for (var i = 0; i < 5; i++)
          Icon(
            Icons.star,
            color: double.parse(rating) >= i + 1 ? Colors.orange : Colors.grey,
            size: 15,
          )
      ],
    );
  }
}
