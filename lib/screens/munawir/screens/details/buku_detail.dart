import 'package:flutter/material.dart';
import 'package:big_project_siskom/screens/munawir/widgets/icon_bulat.dart';
import 'package:big_project_siskom/screens/munawir/screens/details/widgets/detail_teks.dart';

class BukuDetail extends StatelessWidget {
  final Map<String, dynamic> product;

  BukuDetail({this.product});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 50, left: 16, right: 16),
            width: width,
            height: height * 0.7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.keyboard_backspace,
                        size: 40,
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Icon(
                            Icons.search,
                            size: 30,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.shopping_cart,
                            size: 28,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'SPESIAL',
                      style: TextStyle(
                        letterSpacing: 4,
                        fontSize: 15,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 5),
                      color: Colors.green,
                      width: 50,
                      height: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  product['nama'],
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Align(
                  alignment: Alignment.center,
                  child: Hero(
                    tag: product['gambar'],
                    child: Image.network(
                      product['gambar'],
                      height: height * 0.35,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DetailTeks(
                      title: 'HARGA',
                      value: '\R\p\.${product['harga']}',
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    DetailTeks(
                      title: 'WARNA',
                      value: product['warna'],
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    DetailTeks(
                      title: 'TEBAL',
                      value: product['tebal'],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: height * 0.7),
            padding:
                const EdgeInsets.only(left: 16, right: 16, top: 30, bottom: 30),
            width: width,
            height: height * 0.3,
            color: Colors.white,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Sinopsis',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    product['sinopsis'],
                    style: TextStyle(
                      color: Colors.grey,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: height * 0.7 - 25,
            right: 20,
            child: Row(
              children: <Widget>[
                IconBulat(
                  icon: Icons.add_shopping_cart,
                  color: Colors.amber,
                  size: 12,
                ),
                SizedBox(
                  width: 10,
                ),
                IconBulat(
                  icon: Icons.favorite,
                  color: Colors.deepOrange,
                  size: 12,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
