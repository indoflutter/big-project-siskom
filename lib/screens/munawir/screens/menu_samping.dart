import 'package:flutter/material.dart';
import 'package:big_project_siskom/screens/munawir/widgets/teks_miring.dart';
import 'package:big_project_siskom/screens/munawir/models/data.dart';

class MenuSampingWidget extends StatelessWidget {
  final double width;
  final double height;

  MenuSampingWidget({this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        border: Border(
          right: BorderSide(
            width: 1,
            color: Colors.black.withOpacity(0.2),
          ),
        ),
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              top: 50,
              bottom: 50,
            ),
            child: CircleAvatar(
              child: Image.network(profileImg),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                TeksMiring(
                  text: 'Terlaris',
                  isSelected: true,
                ),
                TeksMiring(
                  text: 'Favorit',
                ),
                TeksMiring(
                  text: 'Terbaru',
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 50),
            child: Icon(
              Icons.more_vert,
              size: 35,
            ),
          ),
        ],
      ),
    );
  }
}
