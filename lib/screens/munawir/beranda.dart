import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:big_project_siskom/screens/munawir/models/data.dart';
import 'package:big_project_siskom/screens/munawir/screens/keseluruhan.dart';
import 'package:big_project_siskom/screens/munawir/screens/menu_samping.dart';
import 'package:big_project_siskom/screens/munawir/screens/buku.dart';
import 'package:big_project_siskom/screens/munawir/screens/details/buku_detail.dart';

class PerpusOnline extends StatefulWidget {
  @override
  _PerpusOnlineState createState() => _PerpusOnlineState();
}

class _PerpusOnlineState extends State<PerpusOnline> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    final double menuWidth = 80;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        children: <Widget>[
          MenuSampingWidget(
            width: menuWidth,
            height: height,
          ),
          Container(
            width: width - menuWidth,
            height: height,
            padding: const EdgeInsets.only(
              top: 55,
              right: 16,
              left: 16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.search,
                      size: 30,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Icon(
                      Icons.shopping_cart,
                      size: 28,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Buku Terlaris',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  padding: const EdgeInsets.only(
                    bottom: 20,
                    left: 5,
                    right: 5,
                  ),
                  child: Row(
                    children: <Widget>[
                      for (var i = 0; i < watchData.length; i++)
                        Padding(
                          padding: const EdgeInsets.only(right: 30),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                    transitionDuration:
                                        Duration(milliseconds: 250),
                                    pageBuilder: (context, _, __) => BukuDetail(
                                      product: watchData[i],
                                    ),
                                  ));
                            },
                            child: BukuWidget(
                              product: watchData[i],
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Text(
                  'Kategori',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  padding: const EdgeInsets.only(
                      bottom: 20, top: 20, left: 5, right: 5),
                  child: Row(
                    children: <Widget>[
                      for (var i = 0; i < collectionData.length; i++)
                        Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: Keseluruhan(
                            collection: collectionData[i],
                          ),
                        ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
