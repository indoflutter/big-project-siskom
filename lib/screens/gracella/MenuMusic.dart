import 'dart:io';

import 'package:big_project_siskom/screens/gracella/PlayMusic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_query/flutter_audio_query.dart';

class MenuMusic extends StatefulWidget {
  @override
  _MenuMusicState createState() => _MenuMusicState();
}

class _MenuMusicState extends State<MenuMusic> {
  final FlutterAudioQuery audioQuery = FlutterAudioQuery();
  List<SongInfo> songs = [];
  int currentIndex = 0;

  void initState() {
    super.initState();
    getMenu();
  }

  void getMenu() async {
    songs = await audioQuery.getSongs();
    setState(() {
      songs = songs;
    });
  }

  void changeMenu(bool isNext) {
    if (isNext) {
      if (currentIndex != songs.length - 1) {
        currentIndex++;
      }
    } else {
      if (currentIndex != 0) {
        currentIndex--;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.music_note,
          color: Colors.pink,
        ),
        title: Text(
          "Music Playlist",
          style: TextStyle(color: Colors.pinkAccent),
        ),
      ),
      body: ListView.separated(
        separatorBuilder: (context, index) => Divider(),
        itemCount: songs.length,
        itemBuilder: (context, index) => ListTile(
          leading: CircleAvatar(
            backgroundImage: songs[index].albumArtwork == null
                ? AssetImage('assets/images/background.jpg')
                : FileImage(File(songs[index].albumArtwork)),
          ),
          title: Text(songs[index].title),
          subtitle: Text(songs[index].artist),
          onTap: () {
            currentIndex = index;
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => PlayMusic(
                      changeMenu: changeMenu,
                      songInfo: songs[currentIndex],
                    )));
          },
        ),
      ),
    );
  }
}
