import 'package:flutter/material.dart';

class TodoList extends StatefulWidget {
  @override
  _TodoListState createState() => new _TodoListState();
}

class _TodoListState extends State<TodoList> {
  List<String> _todoList = [];
  TextEditingController _textFieldController = TextEditingController();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Tambahkan Tugas"),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Tuliskan tugasmu disini"),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Tambahkan"),
                onPressed: () {
                  Navigator.of(context).pop();
                  _addTodoItem(_textFieldController.text);
                },
              ),
              new FlatButton(
                child: new Text("Batal"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  void _addTodoItem(String title) {
    setState(() {
      _todoList.add(title);
    });
    _textFieldController.clear();
  }

  List<Widget> _getItems() {
    List<Widget> _todoWidgets = [];
    for (String title in _todoList) {
      _todoWidgets.add(_buildTodoItem(title));
    }
    return _todoWidgets;
  }

  Widget _buildTodoItem(String title) {
    return new ListTile(title: new Text(title));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Daftar Tugas")),
      body: ListView(children: _getItems()),
      floatingActionButton: new FloatingActionButton(
          onPressed: () => _displayDialog(context),
          tooltip: "Tambahkan Tugas",
          child: new Icon(Icons.add)),
    );
  }
}
