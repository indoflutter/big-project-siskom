import 'package:flutter/material.dart';
import 'package:big_project_siskom/screens/suhanty/TodoList.dart';
import 'package:big_project_siskom/screens/suhanty/AboutUs.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  int index =0;
  List<Widget> list = [
    TodoList(),
    AboutUs()
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.greenAccent,
          title: Text("Pengelola Tugas"),
        ),
        body: list[index],
        drawer: MyDrawer(onTap: (context,i){
          setState(() {
            index=i;
            Navigator.pop(context);
          });
        },),
      ),
    );
  }
}

class MyDrawer extends StatelessWidget {

  final Function onTap;

  MyDrawer({
    this.onTap
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width*0.8,
      child: Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(color: Colors.greenAccent),
              child: Padding(
                padding: EdgeInsets.all(6),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: 60,
                      height: 60,
                    ),
                    SizedBox(height: 15),
                    Text(
                      "Pengelola Tugas",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                      color: Colors.white
                    ),  
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.book),
              title: Text("Tugas"),
              onTap: ()=>onTap(context,0),
            ),
            ListTile(
              leading: Icon(Icons.info),
              title: Text("Tentang Kami"),
              onTap: ()=>onTap(context,1),
            ),
          ],
        ),
      ),
    );
  }
}