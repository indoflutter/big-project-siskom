import 'package:big_project_siskom/screens/fahri/model/ParseData.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';

class NewTransactions extends StatefulWidget {
  @override
  _NewTransactionsState createState() => _NewTransactionsState();
}

class _NewTransactionsState extends State<NewTransactions> {
  final int _backgroundColor = 0xfff4f4f2;
  final int _primaryColor = 0xfff4b41a;
  final int _secondaryColor = 0xff143d59;
  Color _textRadioColor1 = Colors.white;
  Color _radioButtonColor1 = Colors.green[300];
  Color _textRadioColor2 = Colors.red;
  Color _radioButtonColor2 = Color(0xfff4f4f2);

  DateTime pickedDate;
  var _radioValue = 1;
  var _myNominal = TextEditingController();

  ParseData note;

  @override
  void initState() {
    super.initState();
    pickedDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width - 100,
        height: 50,
        child: FloatingActionButton.extended(
          backgroundColor: Color(_secondaryColor),
          onPressed: () {
            _saveData();
          },
          label: Text(
            'Save Transaction',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      backgroundColor: Color(_backgroundColor),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Color(_secondaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Color(_primaryColor),
        title: Text(
          'New Transactions',
          style: TextStyle(fontSize: 26, color: Color(_secondaryColor)),
        ),
        centerTitle: true,
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              // radio button
              Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 1,
                        child: FlatButton(
                          color: _radioButtonColor1,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.green[300],
                              width: 2,
                            ),
                          ),
                          child: Row(
                            children: [
                              Radio(
                                value: 1,
                                groupValue: _radioValue,
                                onChanged: null,
                              ),
                              Text(
                                'Pendapatan',
                                style: TextStyle(
                                  color: _textRadioColor1,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              _radioValue = 1;
                              _textRadioColor1 = Colors.white;
                              _radioButtonColor1 = Colors.green[300];
                              _textRadioColor2 = Colors.red;
                              _radioButtonColor2 = Color(0xfff4f4f2);
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        flex: 1,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.red,
                              width: 2,
                            ),
                          ),
                          color: _radioButtonColor2,
                          child: Row(
                            children: [
                              Radio(
                                value: 2,
                                groupValue: _radioValue,
                                onChanged: null,
                              ),
                              Text('Pengeluaran',
                                  style: TextStyle(
                                    color: _textRadioColor2,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ],
                          ),
                          onPressed: () {
                            setState(() {
                              _radioValue = 2;
                              _textRadioColor2 = Colors.white;
                              _radioButtonColor2 = Colors.red;
                              _textRadioColor1 = Colors.green;
                              _radioButtonColor1 = Color(0xfff4f4f2);
                            });
                          },
                        ),
                      ),
                    ],
                  )),
              SizedBox(
                height: 40,
              ),
              // input number nominal
              Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width - 50,
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Nominal',
                      labelStyle: TextStyle(
                        height: 0,
                        color: Color(_secondaryColor),
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.grey[400],
                      )),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color(_secondaryColor),
                        ),
                      )),
                  controller: (_myNominal.text == "" || _myNominal.text == "0")
                      ? (_myNominal..text = '0')
                      : null,
                  style: TextStyle(fontSize: 26, color: Color(_primaryColor)),
                  autofocus: true,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              // input date
              Container(
                width: MediaQuery.of(context).size.width - 30,
                child: FlatButton(
                  onPressed: () {
                    _pickDate();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.date_range,
                        color: Color(_secondaryColor),
                        size: 34,
                      ),
                      Text(
                        DateFormat('  dd-MM-yyyy').format(pickedDate),
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(_secondaryColor),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _saveData() {
    if (_myNominal.text == "") {
      _myNominal..text = "0";
    }
    note = ParseData(null, _myNominal.text, _radioValue.toString(),
        DateFormat('dd-MM-yyyy').format(pickedDate));
    Navigator.pop(context, note);
  }

  _pickDate() async {
    DateTime date = await showDatePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
      initialDate: pickedDate,
    );
    if (date != null)
      setState(() {
        pickedDate = date;
      });
  }
}
