import 'package:big_project_siskom/screens/fahri/Home.dart';
import 'package:big_project_siskom/screens/fahri/Profile.dart';
import 'package:big_project_siskom/screens/fahri/Transactions.dart';
import 'package:flutter/material.dart';

class MainMenu extends StatefulWidget {
  MainMenu({Key key}) : super(key: key);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  int _selectedIndex = 0;
  final int _backgroundColor = 0xfff4f4f2;
  final int _primaryColor = 0xfff4b41a;
  final int _secondaryColor = 0xff143d59;
  static List<Widget> _widgetOptions = <Widget>[
    Home(),
    Transactions(),
    Profile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(_backgroundColor),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Color(_secondaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Color(_primaryColor),
        title: Text(
          'My Finance App',
          style: TextStyle(fontSize: 26, color: Color(_secondaryColor)),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.monetization_on_sharp,
            ),
            label: 'Transaksi',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_circle,
            ),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        iconSize: 34,
        backgroundColor: Color(_backgroundColor),
        unselectedFontSize: 14,
        unselectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
        unselectedIconTheme: IconThemeData(color: Color(_secondaryColor)),
        unselectedItemColor: Color(_secondaryColor),
        selectedFontSize: 14,
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
        selectedIconTheme: IconThemeData(color: Color(_primaryColor)),
        selectedItemColor: Color(_secondaryColor),
        onTap: _onItemTapped,
      ),
    );
  }
}
