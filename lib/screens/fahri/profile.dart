import 'package:flutter/material.dart';
import 'package:googleapis/games/v1.dart';
import 'package:path/path.dart';

class Profile extends StatelessWidget {
  // final int _backgroundColor = 0xfff4f4f2;
  // final int _primaryColor = 0xfff4b41a;
  final int _secondaryColor = 0xff143d59;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.fromLTRB(15.0, 50.0, 15.0, 20.0),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100.0),
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://i.pinimg.com/originals/db/b7/1c/dbb71c9bf78ce2ea2bee074d29576d55.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Text(
                    "username",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 50.0,
          ),
          Container(
            padding: EdgeInsets.only(left: 10.0),
            width: 250,
            height: 100,
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Data Pribadi",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 230.0),
                    ),
                    Text(
                      "Ubah",
                      style: TextStyle(
                        color: Colors.blue[300],
                        fontSize: 15.0,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Username",
                      style: TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "E-mail",
                      style: TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            padding: EdgeInsets.only(left: 10.0),
            width: 250,
            height: 50,
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Setting",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 230.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            padding: EdgeInsets.only(left: 10.0),
            width: 250,
            height: 50,
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Tentang Aplikasi",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 230.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            padding: EdgeInsets.only(left: 10.0),
            width: 250,
            height: 50,
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Keluar",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 230.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
