import 'package:big_project_siskom/screens/fahri/NewTransactions.dart';
import 'package:big_project_siskom/screens/fahri/helpers/DbHelper.dart';
import 'package:big_project_siskom/screens/fahri/model/ParseData.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:division/division.dart';

class Transactions extends StatefulWidget {
  @override
  _TransactionsState createState() => _TransactionsState();
}

class _TransactionsState extends State<Transactions> {
  DbHelper dbHelper = DbHelper();
  int count = 0;
  final int _backgroundColor = 0xfff4f4f2;
  final int _primaryColor = 0xfff4b41a;
  final int _secondaryColor = 0xff143d59;
  int _income = 0;
  int _cost = 0;
  List<ParseData> noteList;

  @override
  void initState() {
    updateListView();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (noteList == null) {
      noteList = List<ParseData>();
    }
    return Scaffold(
      backgroundColor: Color(_backgroundColor),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color(_secondaryColor),
        onPressed: () async {
          var note = await navigateToEntryNote(context, null);
          if (note != null) addNote(note);
        },
        label: Text(
          'Tambah Data',
          style: TextStyle(fontSize: 16),
        ),
        icon: Icon(Icons.add),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Division(
                style: StyleClass()
                  ..width((MediaQuery.of(context).size.width / 2) - 30)
                  ..padding(all: 20)
                  ..backgroundColor(color: Colors.white)
                  ..borderRadius(all: 5.0)
                  ..boxShadow(color: Colors.grey[200], offset: [0, 5], blur: 2)
                  ..align('center')
                  ..alignChild('center'),
                gesture: GestureClass()..onTap(() {}),
                child: Column(
                  children: [
                    Text(
                      _income.toString(),
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Pendapatan',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Division(
                style: StyleClass()
                  ..width((MediaQuery.of(context).size.width / 2) - 30)
                  ..padding(all: 20)
                  ..backgroundColor(color: Colors.white)
                  ..borderRadius(all: 5.0)
                  ..boxShadow(color: Colors.grey[200], offset: [0, 5], blur: 2)
                  ..align('center')
                  ..alignChild('center'),
                gesture: GestureClass()..onTap(() {}),
                child: Column(
                  children: [
                    Text(
                      _cost.toString(),
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Pengeluaran',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Division(
            style: StyleClass()
              ..width((MediaQuery.of(context).size.width) - 50)
              ..padding(all: 20)
              ..backgroundColor(color: Colors.white)
              ..borderRadius(all: 5.0)
              ..boxShadow(color: Colors.grey[200], offset: [0, 5], blur: 2)
              ..align('center')
              ..alignChild('center'),
            gesture: GestureClass()..onTap(() {}),
            child: Column(
              children: [
                Text(
                  (_income - _cost).toString(),
                  style: TextStyle(
                    color: Color(_secondaryColor),
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Keuntungan',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Divider(
            height: 0.0,
            indent: 0.0,
            endIndent: 0.0,
            thickness: 2,
            color: Color(_primaryColor),
          ),
          SizedBox(
            height: 10,
          ),
          createListView(),
        ],
      ),
    );
  }

  Future<ParseData> navigateToEntryNote(
      BuildContext context, ParseData note) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return NewTransactions();
    }));
    return result;
  }

  void countAll() {
    int income = 0;
    int cost = 0;
    for (int index = 0; index < count; index++) {
      (noteList[index].type == "1")
          ? income += int.parse(noteList[index].nominal)
          : cost += int.parse(noteList[index].nominal);
    }
    setState(() {
      _income = income;
      _cost = cost;
    });
  }

  // untuk membuat ListView dan load data pada menu utama jika ada
  Widget createListView() {
    List<Widget> _listTile = new List<Widget>();
    for (int index = 0; index < count; index++) {
      print(index);
      _listTile.add(
        ListTile(
          title: Text(
            (this.noteList[index].type == "1")
                ? "+ " + this.noteList[index].nominal
                : "- " + this.noteList[index].nominal,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: (this.noteList[index].type == "1")
                  ? Colors.green[300]
                  : Colors.red,
            ),
          ),
          subtitle: Text(
            this.noteList[index].date,
            style: TextStyle(fontSize: 14, color: Color(_secondaryColor)),
          ),
          trailing: GestureDetector(
            child: Icon(
              Icons.close,
              color: Colors.red,
            ),
            onTap: () {
              _showAlertDialog().then(
                (value) {
                  if (value) {
                    deleteNote(noteList[index]);
                    Scaffold.of(context).showSnackBar(
                        SnackBar(content: Text('Note telah dihapus')));
                  }
                },
              );
            },
          ),
          onTap: () {},
        ),
      );
      _listTile.add(
        Divider(
          height: 0.0,
          indent: 20.0,
          endIndent: 20.0,
          thickness: 3.5,
          color: Colors.grey[200],
        ),
      );
    }
    return new Column(children: _listTile);
  }

  //buat note baru
  void addNote(ParseData object) async {
    int result = await dbHelper.insert(object);
    if (result > 0) {
      updateListView();
    }
  }

  //delete note di dalam list
  void deleteNote(ParseData object) async {
    int result = await dbHelper.delete(object.id);
    if (result > 0) {
      updateListView();
    }
  }

  //update listview note
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<ParseData>> noteListFuture = dbHelper.getParseDataList();
      noteListFuture.then((noteList) {
        setState(() {
          this.noteList = noteList;
          this.count = noteList.length;
          countAll();
        });
      });
    });
  }

  Future<bool> _showAlertDialog() async {
    bool result = false;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Hapus Transaksi'),
            content: Text('Apakah kamu yakin ingin hapus transaksi ini?'),
            actions: [
              FlatButton(
                onPressed: () {
                  Navigator.pop(context, false);
                },
                child: Text('Close'),
                textColor: Color(_secondaryColor),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context, true);
                },
                child: Text('Hapus'),
                textColor: Color(_secondaryColor),
              ),
            ],
          );
        }).then((value) {
      if (value == null) {
        result = false;
      }
      result = value;
    });
    return result;
  }
}
