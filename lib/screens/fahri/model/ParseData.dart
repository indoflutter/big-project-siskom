class ParseData {
  int _id;
  String _nominal;
  String _type;
  String _date;

  // konstruktor versi 1
  ParseData(this._id, this._nominal, this._type, this._date);

  // konstruktor versi 2: konversi dari Map ke ParseData
  ParseData.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._nominal = map['nominal'];
    this._type = map['type'];
    this._date = map['date'];
  }

  // getter
  // ignore: unnecessary_getters_setters
  int get id => _id;
  // ignore: unnecessary_getters_setters
  String get nominal => _nominal;
  // ignore: unnecessary_getters_setters
  String get type => _type;
  // ignore: unnecessary_getters_setters
  String get date => _date;

  // setter
  // ignore: unnecessary_getters_setters
  set id(int value) {
    _id = value;
  }

  // ignore: unnecessary_getters_setters
  set nominal(String value) {
    _nominal = value;
  }

  // ignore: unnecessary_getters_setters
  set type(String value) {
    _type = value;
  }

  // ignore: unnecessary_getters_setters
  set date(String value) {
    _date = value;
  }

  // konversi dari ParseData ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['nominal'] = nominal;
    map['type'] = type;
    map['date'] = date;
    return map;
  }
}
