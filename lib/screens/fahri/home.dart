import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  // final int _backgroundColor = 0xfff4f4f2;
  // final int _primaryColor = 0xfff4b41a;
  final int _secondaryColor = 0xff143d59;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text(
      "Ups! This Page Under Maintenance",
      style: TextStyle(
        fontSize: 16,
        color: Color(_secondaryColor),
      ),
    ));
  }
}
