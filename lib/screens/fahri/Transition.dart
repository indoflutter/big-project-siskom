import 'dart:async';
import 'package:big_project_siskom/screens/fahri/MainMenu.dart';
import 'package:flutter/material.dart';

class Transition extends StatefulWidget {
  @override
  _TransitionState createState() => _TransitionState();
}

class _TransitionState extends State<Transition>
    with SingleTickerProviderStateMixin {
  final int _primaryColor = 0xfff4b41a;
  final int _secondaryColor = 0xff143d59;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(_primaryColor),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 150,
              backgroundColor: Color(_secondaryColor),
              child: CircleAvatar(
                radius: 140,
                backgroundImage:
                    AssetImage('lib/screens/fahri/assets/images/notebook.png'),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'My Finance App',
              style: TextStyle(
                fontSize: 26,
                fontWeight: FontWeight.bold,
                color: Color(_secondaryColor),
              ),
            ),
          ],
        ),
      ),
    );
  }

  startTimer() async {
    var duration = Duration(seconds: 2);
    return Timer(duration, myNavigation);
  }

  void myNavigation() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MainMenu()));
  }
}
