import 'package:big_project_siskom/screens/Iqbal/Pomodoro.dart';
import 'package:big_project_siskom/screens/fahri/Transition.dart';
import 'package:big_project_siskom/screens/fira/TimerApp.dart';
import 'package:big_project_siskom/screens/gracella/MenuMusic.dart';
import 'package:big_project_siskom/screens/munawir/beranda.dart';
import 'package:big_project_siskom/screens/hutomo/value.dart';
import 'package:big_project_siskom/screens/hutomo/button_card.dart';
import 'package:big_project_siskom/screens/hutomo/google_drive_page.dart';
import 'package:big_project_siskom/screens/rangga/FormRangga.dart';
import 'package:big_project_siskom/screens/suhanty/Main.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Big Project Siskom"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FormRangga()));
            },
            leading: Icon(Icons.arrow_right),
            title: Text("Rangga & Saputra"),
          ),
          FlatButton(
            padding: EdgeInsets.only(top: 12, bottom: 12),
            shape: new RoundedRectangleBorder(
                side: BorderSide(color: Color(0xfff4b41a), width: 2),
                borderRadius: BorderRadius.circular(10.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Fahri & Dwi",
                  style: TextStyle(
                    fontSize: 20,
                    color: Color(0xff143d59),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Color(0xfff4b41a),
                  size: 20,
                ),
              ],
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Transition()));
            },
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text("Suhanty & Hansen"),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Main()));
            },
          ),
          ListTile(
            leading: Icon(Icons.alarm),
            title: Text("Fira, Agamita & Lindini"),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => TimerApp()));
            },
          ),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MenuMusic()));
            },
            leading: Icon(Icons.music_note),
            title: Text("Gracella & jessi"),
          ),
          buildDriveButton(),
          ListTile(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PerpusOnline()));
            },
            leading: Icon(Icons.book),
            title: Text("Munawir - Hendro"),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.green,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Pomodoro()));
        },
        label: Text(
          'Iqbal',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        icon: Icon(Icons.schedule),
      ),
    );
  }

  Widget buildDriveButton() {
    return ButtonCard(
      backgroundColor: Colors.white,
      text: "Hutomo, Hanan & Harits",
      borderColor: mainColor,
      textColor: mainColor,
      splashColor: mainColor,
      margin: 0,
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => GoogleDrivePage()));
      },
    );
  }
}
