import 'package:flutter/material.dart';
import 'package:big_project_siskom/screens/hutomo/value.dart';

class ButtonCard extends StatelessWidget {
  final String text;
  final Function onTap;
  final Color borderColor;
  final Color backgroundColor;
  final Color textColor;
  final double margin;
  final Color splashColor;

// Ini adalah pecobaan mempush melalui bash terminal
  ButtonCard(
      {this.text,
      this.onTap,
      this.borderColor,
      this.backgroundColor,
      this.textColor,
      this.margin,
      this.splashColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: margin ?? defaultMargin),
      height: 50,
      child: RaisedButton(
          padding: EdgeInsets.symmetric(vertical: 12),
          splashColor: splashColor ?? Colors.white,
          elevation: 0,
          child: Text(
            text ?? "",
            style: TextStyle(
                color: textColor ?? Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(color: borderColor ?? mainColor)),
          color: backgroundColor ?? mainColor,
          onPressed: () async {
            if (onTap != null) {
              onTap();
            }
          }),
    );
  }
}
