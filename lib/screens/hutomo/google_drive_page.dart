import 'dart:io';

import 'package:big_project_siskom/screens/hutomo/camera_page.dart';
import 'package:big_project_siskom/screens/hutomo/google_drive_services.dart';
import 'package:big_project_siskom/screens/hutomo/value.dart';
import 'package:big_project_siskom/screens/hutomo/button_card.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class GoogleDrivePage extends StatefulWidget {
  @override
  _GoogleDrivePageState createState() => _GoogleDrivePageState();
}

class _GoogleDrivePageState extends State<GoogleDrivePage> {
  final googleDrive = GoogleDriveServices();
  File imageFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.grey),
        elevation: 0,
        centerTitle: true,
        title: Text("K-Gove",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20)),
      ),
      body: ListView(
        children: [
          Text("Keilmuan Google Drive",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black.withOpacity(0.5), fontSize: 20)),
          SizedBox(
            height: 20,
          ),
          buildImageContainer(),
          SizedBox(
            height: 10,
          ),
          buildButtonTakePicture(),
          (imageFile != null)
              ? SizedBox(
                  height: 15,
                )
              : SizedBox(),
          (imageFile != null) ? buildButtonUpload() : SizedBox(),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  //Widget Image Container
  Widget buildImageContainer() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: defaultMargin),
      width: MediaQuery.of(context).size.width,
      height: 450,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.grey),
      child: (imageFile != null)
          ? ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.file(
                imageFile,
                fit: BoxFit.cover,
              ),
            )
          : InkWell(
              onTap: () async {
                imageFile = await Navigator.push<File>(
                    context, MaterialPageRoute(builder: (_) => CameraPage()));
                setState(() {});
              },
              child: Center(
                child: Icon(
                  Icons.add_a_photo,
                  size: 40,
                ),
              ),
            ),
    );
  }

  //Widget button TakePicture
  Widget buildButtonTakePicture() {
    return ButtonCard(
      backgroundColor: Colors.white,
      text: (imageFile == null) ? "Ambil foto" : "Ganti",
      borderColor: mainColor,
      textColor: mainColor,
      splashColor: mainColor,
      onTap: () async {
        imageFile = await Navigator.push<File>(
            context, MaterialPageRoute(builder: (_) => CameraPage()));
        setState(() {});
      },
    );
  }

  //Widget button Upload
  Widget buildButtonUpload() {
    return ButtonCard(
        text: "Upload",
        onTap: () async {
          onLoading();
        });
  }

  //Upload Function
  void onLoading() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          insetAnimationDuration: Duration(milliseconds: 150),
          insetPadding: EdgeInsets.all(50),
          insetAnimationCurve: Curves.bounceIn,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [loadingIndicator],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 10, 10),
                child: Text(
                  "Uploading...",
                  style: TextStyle(fontSize: 25),
                ),
              ),
            ],
          ),
        );
      },
    );

    await googleDrive.upload(imageFile).then((value) {
      Navigator.pop(context);
      Alert(
        context: context,
        type: AlertType.success,
        title: "",
        desc: "Berhasil meng-upload gambar",
        buttons: [
          DialogButton(
              color: mainColor,
              child: Text(
                "Kembali",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ).show();
    });
  }
}
