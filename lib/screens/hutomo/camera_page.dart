import 'dart:io';

import 'package:big_project_siskom/main.dart';
import 'package:big_project_siskom/screens/hutomo/value.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class CameraPage extends StatefulWidget {
  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  CameraController cameraController;

  Future<void> initializeCamera() async {
    cameraController = CameraController(cameras[0], ResolutionPreset.medium);
    await cameraController.initialize();
  }

  @override
  void dispose() {
    cameraController.dispose();
    super.dispose();
  }

  Future<File> takePicture() async {
    Directory root = await getTemporaryDirectory();
    String directoryPath = "${root.path}/Guided_Camera";
    await Directory(directoryPath).create(recursive: true);
    String filePath = "$directoryPath/${DateTime.now()}.jpg";

    try {
      await cameraController.takePicture(filePath);
    } catch (e) {
      return null;
    }

    return File(filePath);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: FutureBuilder(
          future: initializeCamera(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return buildCamera();
            } else {
              return loadingIndicator;
            }
          },
        ));
  }

  //Widget Camera
  Widget buildCamera() {
    return Column(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.width /
              cameraController.value.aspectRatio,
          child: CameraPreview(cameraController),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: defaultMargin),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(100),
                onTap: () async {
                  Navigator.pop(
                    context,
                  );
                },
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white)),
                  child: Icon(
                    Icons.close,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white)),
                  child: Container(
                    width: 70,
                    height: 70,
                    child: RaisedButton(
                        onPressed: () async {
                          if (!cameraController.value.isTakingPicture) {
                            File result = await takePicture();
                            Navigator.pop(context, result);
                          }
                        },
                        shape: CircleBorder(),
                        color: Colors.red),
                  )),
              Container(width: 70, height: 70, child: SizedBox()),
            ],
          ),
        )
      ],
    );
  }
}
