import 'dart:convert';

import 'package:big_project_siskom/screens/fahrizal/detail.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _post = [];
  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aplikasi Berita'),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: _post.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Container(
              color: Colors.white,
              height: 100,
              width: 100,
              child: _post[index]['urlToImage'] != null
                  ? Image.network(
                      _post[index]['urlToImage'],
                      width: 100,
                      fit: BoxFit.cover,
                    )
                  : Center(),
            ),
            title: Text(
              '${_post[index]['title']}',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            subtitle: Text(
              '${_post[index]['description']}',
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Detail(
                            url: _post[index]['url'],
                            title: _post[index]['title'],
                            content: _post[index]['content'],
                            publishedAt: _post[index]['publishedAt'],
                            author: _post[index]['author'],
                            urlToImage: _post[index]['urlToImage'],
                          )));
            },
          );
        },
      ),
    );
  }

  Future _getData() async {
    try {
      final response = await http.get(
          'https://newsapi.org/v2/top-headlines?country=id&apiKey=6a51f958f9104bb7a15e1a63710e26b3');

      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        setState(() {
          _post = data['articles'];
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
