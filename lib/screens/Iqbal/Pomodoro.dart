import 'dart:async';


import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Pomodoro extends StatefulWidget {
  @override
  _PomodoroState createState() => _PomodoroState();
}

class _PomodoroState extends State<Pomodoro> {
  var timerStream;
  var timerSubscription;
  Timer _timer;
  AudioCache cache = AudioCache();
  AudioPlayer player = AudioPlayer();
  Duration pomodoroTime = Duration(minutes: 0);
  Duration pilihanWaktu = Duration(minutes: 0);
  Duration satuDetik = Duration(seconds: 1);
  Duration zero = const Duration(seconds: 0);
  bool isRun = false;
  bool nilaiNol = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        backgroundColor: hexToColor("#92817a"),
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: hexToColor("#393e46"),
          ),
          title: Text(
            "Pomodoro",
            style: TextStyle(
              color: hexToColor("#393e46"),
              fontWeight: FontWeight.bold,
            ),
            ),
          backgroundColor: hexToColor("#8db596"),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularPercentIndicator(
                  radius: 260.0,
                  lineWidth: 15,
                  animation: true,
                  percent: 1.0,
                  circularStrokeCap: CircularStrokeCap.round,
                  reverse: true,
                  center: Text(
                    "${_waktu(pomodoroTime)}",
                    style: TextStyle(
                      fontSize: 70.0,
                      color: hexToColor("#393e46"),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  progressColor: hexToColor("#bedbbb"),
                  backgroundColor: hexToColor("#92817a"),
                ),
                SizedBox(height: 80.0,),
                
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 115.0,
                      height: 45.0,
                      child: RaisedButton(
                        onPressed: (){
                          selectTimer(50);
                          resetTimer();
                        },
                        color: hexToColor("#8db596"),
                        child: Text(
                          '50 Menit',
                          style: TextStyle(
                            color: hexToColor("#393e46"),
                            fontSize: 20.0,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14.0)
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    SizedBox(
                      width: 115.0,
                      height: 45.0,
                      child: RaisedButton(
                        onPressed: (){
                          selectTimer(25);
                          resetTimer();
                        },
                        color: hexToColor("#8db596"),
                        child: Text(
                          '25 Menit',
                          style: TextStyle(
                            color: hexToColor("#393e46"),
                            fontSize: 20.0,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14.0)
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    SizedBox(
                      width: 115.0,
                      height: 45.0,
                      child: RaisedButton(
                        onPressed: () {
                          selectTimer(5);
                          resetTimer();

                        },
                        color: hexToColor("#8db596"),
                        child: Text(
                          '5 Menit',
                          style: TextStyle(
                            color: hexToColor("#393e46"),
                            fontSize: 20.0,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14.0)
                        ),
                      ),
                    ),      
                  ],              
                ),
                SizedBox(height: 20.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 140.0,
                      height: 80.0,
                      child: RaisedButton(
                        onPressed: () {
                          mulaiTimer();
                        },
                        color: hexToColor("#8db596"),
                        child: Text(
                          'Start',
                          style: TextStyle(
                            color: hexToColor("#393e46"),
                            fontSize: 20.0,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0)
                        ),
                      ),
                    ),
                    SizedBox(width: 20.0,),
                    SizedBox(
                      width: 140.0,
                      height: 80.0,
                      child: RaisedButton(
                        onPressed: () {
                        selectTimer(0);
                        stopSound();
                        resetTimer();
                        },
                        color: hexToColor("#8db596"),
                        child: Text(
                          'Reset',
                          style: TextStyle(
                            color: hexToColor("#393e46"),
                            fontSize: 20.0,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0)
                        ),
                      ),
                ),
                  ] 
                ),
                SizedBox(width: 20.0,),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<void> _showMyDialog() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    barrierColor: Colors.black,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: ()async => false,
        child: AlertDialog(
          backgroundColor: hexToColor('#f4d9c6'),
          title: Text('${_waktu(pilihanWaktu)} menit'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('${_waktu(pilihanWaktu)} menit telah berlalu'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Matikan Alarm'),
              onPressed: () {
                stopSound();
                Navigator.of(context).pop();      
              },
            ),
          ],
        ),
      );
    },
  );
  }
  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: hexToColor('#f4d9c6'),
        title: Text('Apakah Anda Yakin Ingin keluar?'),
        content: Text('keluar sekarang?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Tidak'),
          ),
          FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text('Iya'),
          ),
        ],
      ),
    )) ?? false;
  }


  void mulaiTimer() {
    if (isRun == false && nilaiNol == false) {
      print("Mulai");
      isRun = true;
      const detik = const Duration(seconds: 1);
      _timer = new Timer.periodic(
          detik,
          (Timer timer) => setState(() {
                if (pomodoroTime <= zero) {
                  timer.cancel();
                  playFile();
                  _showMyDialog();
                } else {
                  pomodoroTime = pomodoroTime - satuDetik;
                }
              }));
    }
  }
  void reset(int waktu){
    pomodoroTime = Duration(minutes: waktu);
    pilihanWaktu = Duration(minutes: waktu);
  }
  void selectTimer(int pilihan){
    if (pilihan >= 1) {
      setState(() {
        reset(pilihan);
        nilaiNol = false;
    });
     } else{
      setState(() {
        reset(pilihan);
        nilaiNol = true;
        
      });    
    }
  }
  void resetTimer() {
    stopSound();
    if (isRun == true) {
      isRun = false;
      _timer.cancel(); 
      _timer = null;   
    }
  }
  @override
  void dispose() {
    _timer?.cancel();
    player?.stop();
    super.dispose();
  }

  String _waktu(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitsMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitsSecond = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitsMinutes:$twoDigitsSecond";
  }
  void playFile()async {
    player = await cache.loop('Alarm.mp3');
  }
  void stopSound(){
    player?.stop();
  }

  Color hexToColor(String hexCode) {
    return Color(int.parse(hexCode.substring(1, 7), radix: 16) + 0xFF000000);
  }
}
